const http = require('http');
const express = require('express')
const path = require('path')
const cors = require('cors')
const app = express()
const PORT = process.env.PORT || 5000;

var server = http.createServer(app)

var io = require('socket.io')(server, {
    handlePreflightRequest: (req, res) => {
        const headers = {
            "Access-Control-Allow-Headers": "Content-Type, Authorization, jwt",
            "Access-Control-Allow-Origin": req.headers.origin, //or the specific origin you want to give access to,
            "Access-Control-Allow-Credentials": true
        };
        res.writeHead(200, headers);
        res.end();
    }});

app.use(cors())

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/web/index.html');
});

app.use(express.static(path.join(__dirname, 'web')))

let users = [];

io.on('connect', socket => {

  if (socket.handshake.headers["jwt"]){
    console.log("jwt found: " + socket.handshake.headers["jwt"])
  }
  
  socket.send({user:"SERVER",message:'You have connected to craig chat server!'});
  socket.broadcast.emit('message', "someone connected")

  socket.on('message', (data) => {
    console.log(data.user + ": " + data.message)

    if(users.indexOf(data.user) < 0){
      console.log("new user: "+ data.user)
      users.push(data.user)

      //tell all clients
      socket.emit('addUser', data.user)
      socket.broadcast.emit('addUser', data.user)
    }

    socket.emit('message', data)
    socket.broadcast.emit('message', data)
  });

  socket.on('username', (data) => {
    console.log(data)
    socket.broadcast.emit('userAdd', data)
  });

  socket.on('disconnect', () => {
    socket.emit('message', "Someone left ...")
  });

});


server.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})